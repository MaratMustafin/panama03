<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Route::get('/welcome', function () {
    return ['foo' => 'bar'];
});

Route::get('/test', function () {
    // double brace replace htmlspecialchars in code
    // $name = htmlspecialchars(request('name'),ENT_QUOTES);
    // e function to make htmlspecialchars
    // {!! $name !!} equal <?= $name
    $name = request('name');
    return view('test', [
        'name' => $name,
    ]);
});
Route::get('/posts/{post}', function ($post) {
    $posts = [
        'my-first-post' => 'Hello #1',
        'my-second-post' => 'Hello #2',
    ];
    if (!array_key_exists($post, $posts)) {
        abort(404, "Soorrry");
    }
    return view('post', [
        'post' => $posts[$post],
    ]);
});
Route::get('/articles', 'ArticlesController@index')->name('articles.index');
Route::post('/articles', 'ArticlesController@store');
Route::get('/articles/create', 'ArticlesController@create');
Route::get('/articles/{article}', 'ArticlesController@show')->name('articles.show');
Route::get('/articles/{article}/edit', 'ArticlesController@edit');
Route::put('/articles/{article}', 'ArticlesController@update');

Route::get('/event/{event}', 'EventsController@show');

Route::get('/posts/{name}', 'PostsController@show');

Route::get('/contact', function () {
    return view('contact');
});
Route::get('/about', function () {

    return view('about', [
        'articles' => App\Article::take(2)->latest()->get(),
    ]);
});
