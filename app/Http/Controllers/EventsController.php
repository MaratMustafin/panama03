<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EventsController extends Controller
{
    public function show($event) 
    {
        $events = [
            'my-first-event' => 'First event',
            'my-second-event' => 'Second event'
        ];
        if (!array_key_exists($event,$events)) {
            abort(404, "Sorry we not found");
        }

        return view('events', [
            'event' => $events[$event]
        ]);       
    }
}
