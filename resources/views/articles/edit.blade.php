@extends('layout')

@section('content')
<form action="/articles/{{ $article->id }}" method="POST">
    @csrf
    @method('PUT')
    <label for="title">Title</label>
    <input type="text" name="title" id="" value="{{ $article->title }}"><br>
    <label for="excerpt">Excerpt</label>
    <textarea name="excerpt" id="" cols="30" rows="10" >{{ $article->excerpt }}</textarea><br>
    <label for="body">Description</label>
    <textarea name="body" id="" cols="30" rows="10">{{ $article->body }}</textarea><br>
    <button type="submit">Send</button>
</form>
@endsection