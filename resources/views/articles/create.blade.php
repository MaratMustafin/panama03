@extends('layout')

@section('content')
    <form action="/articles" method="POST">
        @csrf
        <label for="title">Title</label>
        <input 
            style="@error('title') background-color: red @enderror" 
            type="text" 
            name="title" 
            id=""
            value="{{ old('title') }}"><br>
        @error('title')
            <p>{{ $errors->first('title') }}</p>
        @enderror
        <label for="excerpt">Excerpt</label>
        <textarea 
            style="@error('excerpt') background-color: red @enderror" 
            name="excerpt" 
            id="" 
            cols="30" 
            rows="10">{{ old('excerpt') }}</textarea><br>
        @error('excerpt') 
            <p>{{ $errors->first('excerpt') }}</p>
        @enderror
        <label for="body">Description</label>
        <textarea 
            style="@error('body') background-color: red @enderror" 
            name="body" 
            id="" 
            cols="30" 
            rows="10">{{ old('body') }}</textarea><br>
            @error('body') 
            <p>{{ $errors->first('body') }}</p>
            @enderror        
        <label for="tags">Select tags</label>
        <select name="tags[]" id="tags" multiple>
            @foreach($tags as $tag)
                <option value="{{ $tag->id }}">{{ $tag->name }}</option>
            @endforeach
        </select>
        <br>
        @error('tags') 
        <p>{{ $message }}</p>
        @enderror
        <button type="submit">Send</button>
    </form>
@endsection
